
### RAF migration node

  Migration Backend
     * réécriture des webservices
          - DIAG     : DB:v / WS:TODO
          - LOGIN    : v
          - SITES    : v
          - COMMUNES : v
          - ETATS    : v
          - REMARQUES: v
          - JOURNAL  : v but optimizations needed
          - PHOTOS   : TODO
     * export mysql :
          - SITES    : v
          - COMMUNES : v
          - ETATS    : déjà existant = simple get
          - REMARQUES: v
          - PHOTOS   : v


  - Diary
      + apply initial filter in template ?
      + optimizations needed

  - Photos
     - backend
     - fix debounce in Map.js
     - ajout de ces événements au journal

  - Filter
    - fix departements sort (both selected and unselected)
    - Improve UI/UX

  - https

  - remove jquery
  
  - media queries for mobile devices
  
  - CSS transitions wherever possible (translate, opacity)
  
  - CSV departement 59
  
  - extract events to separate files, that can be concatenated and requested by other module, to improve lazy loading of secondary   importance modules
  
  - remove any anonymous functions
  
  - find places around me

  - Journal: relative timestamps ??? http://timeago.yarp.com/

  - Ne pas attendre l'evenement websocket pour supprimer le tooltip local precedent

  - saisie-remarque : fermeture si clic en dehors et vide, sauf si contenu saisi (popup de confirmation ?)




### Version 0.2.1 - Mise en prod le 21/10/2015

- Ajustements suite au nouveau layout de google maps

- Services d'export de la base de données (pour migration php/mysql->node/mongodb)


### Version 0.2.0 - Mise en prod le 10/07/2015

- Page d'aide

- Notes de versions

- Bugfixes
     - Journal: Le filtre ne s'appliquait pas sur les evenements provenant du temps reel
     - Journal: Ajout d'une remarque en temps reel = le numero de departement n'etait pas indique
     - Correction du bug a l'annulation du deplacement en cas de focus sur un autre site
     - Recherche: Correction de la regexp pour les coordonnees geodesique

- Journal: Lors de l'ajout d'un lieu, montrer le nom plutot que l'adresse
- Recherche: Support des coordonnees sexagesimal (du type: 48&deg;49'53.36"N 0&deg;29'38.70"E)



### Version 0.1 - Mise en prod le 23/06/2015

#### Front

- Login

- Ecran de chargement

- Recuperation des sites
     - Tous
     - Par departements
     - Par id

- Creation d'un site
     - Normalisation de la commune pour la recherche du code insee
     - Choix du nom
     - Choix de l'etat

- Fiche info
     - Zoom sur site
     - Icone link pour partager un lien direct vers un site
     - Lien vers la fiche complete sur le "site source"
     - Afficher l'auteur et date de mise a jour de l'etat si != import
     - Afficher les remarques

- Configurer CKEditor pour la saisie de remarques

- Mise a jour d'un site
     - Etat
     - Position
     - Ecrire une remarque

- Street view minimap

- Drag/drop site pour corriger la position
     - Visualisation de la reverse address
     - Possibilite d'enregistrer ou d'annuler le deplacement

- Rond differenciant le site selectionne

- Push
     - Position
     - Etat
     - Tooltip indiquant qui est sur quelle fiche
         - gestion de plusieurs utilisateurs sur un meme site
         - fermeture tooltip lors de la fermeture de la fenetre

- Champ de recherche par adresse
     - Support de coordonnees au format geodesique (du type: 48.831489, -0.494083)

- Refactoring :
     - Extraire autant de modules que possible
     - Extraire les templates, les charger par le module correspondant (idem pour le css)

- Journal d'evenements
     - Possibilite de le plier/deplier
     - Lorsqu'il est plie, compteur de nouveaux evenements
     - Remarques
     - Pagination
     - Filtre par types d'evenements

- Diag
     - DB
     - WS

- Filtres
     - par departements
     - par etats

- Notifications

- Photos
     - Upload de photos
     - Gallerie de ces photos

- Deconnexion


#### Back

- Batch java pour telecharger tous les fichiers CSV
- Alimentation base de donnees
- Geocodage WGS84

- URL Rewritting pour les appels REST
- Verification session sur chaque requete serveur
- Check de sante (diag)


#### Websocket

   - Remplacement du serveur php par node+socket-io


### Démarrage du projet - 12/11/2014