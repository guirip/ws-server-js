(function(){
    "use strict";

    var app  = require('express')(),
        http = require('http').Server(app),
        io   = require('socket.io')(http),
        _    = require('lodash'),
        port = process.env.port ? process.env.port : require('./config.json').port;

    /**
     * Return a current date & time label
     * @private
     */
    var getCurrentDateTimeLabel = function(){
        var date = new Date();
        return date.toLocaleDateString() + ' ' + date.toLocaleTimeString();
    };

    /**
     * Log something prepending date & time
     * @param string
     * @private
     */
    var log = function(string){
        console.log("[" + getCurrentDateTimeLabel() + "] " + string);
    };

    /**
     * Handle shutdown
     * @param e
     * @private
     */
    var exitHandler =  function(e){
        _.debounce(proceedExit, 100)(e);
    };
    var proceedExit = function(e){
        if (e && e.stack){
            log(e.stack);
        }
        log("Web-socket server shutdown.");
        http.close();
    };

    // do something when app is closing
    process.on('exit', exitHandler);

    // catches ctrl+c event
    process.on('SIGINT', exitHandler);

    // catches uncaught exceptions
    process.on('uncaughtException', exitHandler);

    process.on('close', function() {
        process.exit();
    });

    // Start server

    app.get('/ping', function(req, res){
        res.send('pong');
    });

    io.on('connection', function(socket){
        console.log('a user connected', socket.id);

        socket.on('disconnect', function(){
            console.log('user disconnected', socket.id);
        });

        socket.on('event', function(msg){
            io.emit('event', msg);
        });
    });


    http.listen(port, function(){
        log('Web socket server started');
        log('Listening on *:'+port);
    });

})();

